# filer

The main use case of this utility is best described with an example. Thus, here
is that example:

Assume you've got a software project (language is irrelevant) and, to build, you
are using makefiles. In makefiles, you have a few options to specify all of your
source files, etc.

One option is to list each source file from your project in the makefile. It
would probably look something like this:
```
SRC = file1.cpp file2.cpp file3.cpp ... fileN.cpp
```
As you can imagine, this can get unweildy very quickly with any fairly large
project (imagine that your project only had 50 files. If you had one per line
that would be 50 lines in your makefile with just filenames -- *whew*!).

Another option might be to do something like this (assuming a C++ project):
```
SRC = $(shell ls *.cpp)
```
But then, that looks ugly... and what if you didn't want all the files to be
included? Let's say your application has a free version and a paid version; you
might need to include different files (or not include files) based on whether
you are building the paid or the free version.

Another option could be to write all of the source file filenames to a separate
file and cat them into the `SRC` variable, something like this:
```
SRC = $(shell cat source.txt)
```
This could actually be a good option except that `cat` has no "intelligence".
That is, you would never be able to have extra functionality such as putting
comments into those files to let you know anything about any of the files. You
would never be able to "include" other `source.txt` files, if you wanted to. So
on and so forth.

And so `filer` comes to the rescue! With `filer` you can write the above `SRC`
line like this:
```
SRC = $(shell filer source.txt)
```

Note: The example relating to a paid version and a free version of your project,
could be written like this using `filer`:
```
ifeq ($(PAID_VERSION), 1)
SRC = $(shell filer source_paid.txt)
else
SRC = $(shell filer source_free.txt)
endif
```

Here are some of the benefits of using
`filer` in your build toolchain:
* You can write, what we call, *filename files*.
    * *filename files* are files that list the filenames of every file to be
      included in the build of a specific file
    * These types of files provide the added benefit that we can now track them
      with version control (you are using version control, right?)
* You can include comments in your *filename files*.
* You can include *filename files* in other *filename files*.
    * A benefit of this approach is that you can separate your project into
      different directories each containing their own *filename file* and you
      can have a "master" *filename file* that starts everything off.
* *__And many more!__*
