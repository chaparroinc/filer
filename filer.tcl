#!/usr/bin/env tclsh

proc usage { arg } {
    puts "error: the file `$arg` does not exist"
    puts "usage: filer ?filename1? ?filename2? ?...? ?filenameN?"
}

proc is_file { arg } {
    return [ file exists $arg ]
}

proc get_file_lines { fname } {
    set file_size   [ file  size $fname ]
    set file_handle [ open  $fname "r" ]
    set file_data   [ read  $file_handle $file_size ]
    set lines       [ split $file_data "\n" ]
    close $file_handle
    return $lines
}

proc remove_comment { line } {
    if { [ string match "*#*" $line ] } {
        set line [ lindex [ split $line " #" ] 0 ]
    }
    return $line
}

proc print_line { line } {
    if { [ string length $line ] } {
        puts -nonewline "$line "
    }
}

proc process_line { line } {
    set line [ remove_comment $line ]
    print_line $line
}

proc process_lines { lines } {
    foreach line $lines {
        process_line $line
    }
}

foreach fname $::argv {
    if { [ is_file $fname ] } {
        set lines [ get_file_lines $fname ]
        process_lines $lines
    } else {
        usage $fname
    }
}
